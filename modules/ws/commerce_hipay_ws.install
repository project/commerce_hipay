<?php

/**
 * Implements hook_requirements().
 */
function commerce_hipay_ws_requirements($phase) {
  $t = get_t();
  $requirements = array();

  // Add cURL requirement only if SimpleTest is not installed,
  // to avoid multiple cURL rows.
  if (!module_exists('simpletest')) {
    $has_curl = function_exists('curl_init');

    $requirements['commerce_hipay_ws_curl'] = array(
      'title' => $t('Hipay Wallet: cURL'),
      'value' => $has_curl ? $t('cURL library enabled') : $t('cURL library not found'),
    );

    if (!$has_curl) {
      $requirements['commerce_hipay_ws_curl'] += array(
        'severity' => REQUIREMENT_ERROR,
        'description' => $t("Hipay Wallet requires the PHP <a href='!curl_url'>cURL</a> library.", array('!curl_url' => 'http://php.net/manual/en/curl.setup.php')),
      );
    }
  }

  // Verify that the SoapClient class is found.
  $has_soap = class_exists('SoapClient');

  $requirements['commerce_hipay_ws_soap'] = array(
    'title' => $t('Hipay Wallet: SoapClient'),
    'value' => $has_soap ? $t('SoapClient class found') : $t('SoapClient class not found'),
  );

  if (!$has_soap) {
    $requirements['commerce_hipay_ws_soap'] += array(
      'severity' => REQUIREMENT_ERROR,
      'description' => $t("Hipay WS requires the PHP <a href='!soap_url'>SoapClient</a> extension.", array('!soap_url' => 'http://php.net/manual/en/soap.setup.php')),
    );
  }

  return $requirements;
}

/**
 * Implements hook_schema().
 */
function commerce_hipay_ws_schema() {
  $schema = array();

  module_load_include('inc', 'commerce_hipay_ws', 'includes/entities/commerce_hipay_ws.entity.user_account');
  $schema += commerce_hipay_ws_user_account_schema();

  module_load_include('inc', 'commerce_hipay_ws', 'includes/entities/commerce_hipay_ws.entity.bank_account');
  $schema += commerce_hipay_ws_bank_account_schema();

  module_load_include('inc', 'commerce_hipay_ws', 'includes/entities/commerce_hipay_ws.entity.file');
  $schema += commerce_hipay_ws_file_schema();

  module_load_include('inc', 'commerce_hipay_ws', 'includes/entities/commerce_hipay_ws.entity.transfer');
  $schema += commerce_hipay_ws_transfer_schema();

  module_load_include('inc', 'commerce_hipay_ws', 'includes/entities/commerce_hipay_ws.entity.withdrawal');
  $schema += commerce_hipay_ws_withdrawal_schema();

  return $schema;
}

/**
 * Implements hook_install().
 */
function commerce_hipay_ws_install() {
  module_load_include('inc', 'commerce_hipay_ws', 'includes/entities/commerce_hipay_ws.entity.user_account');
  commerce_hipay_ws_user_account_create_fields();

  module_load_include('inc', 'commerce_hipay_ws', 'includes/entities/commerce_hipay_ws.entity.bank_account');
  commerce_hipay_ws_bank_account_create_fields();

  module_load_include('inc', 'commerce_hipay_ws', 'includes/entities/commerce_hipay_ws.entity.transfer');
  commerce_hipay_ws_transfer_create_fields();

  module_load_include('inc', 'commerce_hipay_ws', 'includes/entities/commerce_hipay_ws.entity.withdrawal');
  commerce_hipay_ws_withdrawal_create_fields();
}

/**
 * Implements hook_uninstall().
 */
function commerce_hipay_ws_uninstall() {
  module_load_include('inc', 'commerce_hipay_ws', 'includes/entities/commerce_hipay_ws.entity.user_account');
  commerce_hipay_ws_user_account_delete_fields();

  module_load_include('inc', 'commerce_hipay_ws', 'includes/entities/commerce_hipay_ws.entity.bank_account');
  commerce_hipay_ws_bank_account_delete_fields();

  module_load_include('inc', 'commerce_hipay_ws', 'includes/entities/commerce_hipay_ws.entity.transfer');
  commerce_hipay_ws_transfer_delete_fields();

  module_load_include('inc', 'commerce_hipay_ws', 'includes/entities/commerce_hipay_ws.entity.withdrawal');
  commerce_hipay_ws_withdrawal_delete_fields();
}
